import { User } from "./User";

export interface AuthResult {
    result : UserWithToken
    statusCode : number
}

interface UserWithToken {
    token : string;
    user : User
}