import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    //request : Contient la requête interceptée
    //next : Permet de continuer la requête

    //Vérifier si on a un token
    let token : string | null = localStorage.getItem('token');
    if(token && token != ''){
      //Si oui, on clone la requête et on rajoute dans les headers et continuer la requête (clonée)
      let requestClone = request.clone({ setHeaders : { "Authorization" : `Bearer ${token}` } })
      return next.handle(requestClone);

    }

    //Si pas, on continue la requête
    return next.handle(request);
  }

  //L'intercepteur a le mérite d'exister mais il ne faut pas oublier d'aller l'injecter dans le module de votre choix
}
